class TempLayout < ActiveRecord::Base
attr_accessible :nombre, :celular, :email, :estadoderesidencia,  :edadetitular, :edaddeconyugue, :numdehijosconedadde24ymenores, :formadepago, :edaddehijo1mayora24, :edaddehijo2mayora24, :edaddehijo3mayora24, :edaddehijo4mayora24, :edaddelpadredeltitular, :edaddelamadredeltitular, :edaddelasuegradeltitular, :edaddelasuegrodeltitular, :plan, :viajes, :cashemergencia
before_create :set_plan

private
	def set_plan
		if self.viajes == '4 o más'
			self.plan = 'WA'
		else
			self.plan = 'PA'
		end
		
	end
    

end
