class Plan < ActiveRecord::Base
  attr_accessible :acompanante_noche, :ambulancia, :area_cobertura, :cobertura_maxima_anual_asegurado, :complicaciones_embarazo, :deducible, :elim_deducible, :estudios_laboratorio, :examenes_sin_patologia, :extension_coberturas, :honorarios_medicos, :hospitalizacion, :maternidad, :medicamentos, :nombre, :podologia, :reduc_deducible, :terapias, :trasplante_organos
end
