class Layout < ActiveRecord::Base
attr_accessible :nombre, :celular, :email, :estadoderesidencia,  :edadetitular, :edaddeconyugue, :numdehijosconedadde24ymenores, :formadepago, :edaddehijo1mayora24, :edaddehijo2mayora24, :edaddehijo3mayora24, :edaddehijo4mayora24, :edaddelpadredeltitular, :edaddelamadredeltitular, :edaddelasuegradeltitular, :edaddelasuegrodeltitular, :plan, :viajes, :cashemergencia
before_create :set_plan
after_create :send_signup_email
private
	def set_plan
		if self.viajes == '4 o más'
			self.plan = 'WA'
		else
			self.plan = 'PA'
		end
		
	end
    def send_signup_email #Sends Welcome email to user and notification to admin of new signup
      SignupMailer.signup_email(self).deliver
      
    end

end
