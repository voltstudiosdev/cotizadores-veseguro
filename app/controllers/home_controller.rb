class HomeController < ApplicationController
  def index
  	@layout = Layout.new


  end
  def agentes
    @layout = Aglayout.new


  end

  def cotizacionpalig
    @layout = Layout.last
    
    if @layout.formadepago == 'Anual'
      @formapago = 1
      @rpf = 0
      @formapagosubsecuentes = 0/@formapago
    elsif @layout.formadepago == 'Semestral'
       @formapago = 2
       @rpf = 0.045
       @formapagosubsecuentes = 0.5
    elsif @layout.formadepago == 'Trimestral'
       @formapago = 4
       @rpf = 0.075
       @formapagosubsecuentes = 0.25
    end
      
    @iva = 0.16
    @prod = @layout.plan
    if @prod == 'PA'
      @plan = Plan.where(nombre: 'Preferred Access').first
    elsif @prod == 'WA'
      @plan = Plan.where(nombre: 'World Access').first
    end
    
    case @layout.estadoderesidencia
    when "Aguascalientes"
      @zona = 5
    when "Baja California (Ensenada)"
      @zona = 1
    when "Baja California (Mexicalli)"
      @zona = 1
    when "Baja California (Playas de Rosarito)"
      @zona = 1
    when "Baja California (Rodolfo Sánchez T.  (Maneadero))"
      @zona = 1
    when "Baja California (San Felipe)"
      @zona = 1
    when "Baja California (Tecate)"
      @zona = 1
    when "Baja California (Tijuana)"
      @zona = 1
    when "Baja California (OTRAS CIUDADES)"
      @zona = 2
    when "Baja California Sur"
      @zona = 2
    when "Campeche"
      @zona = 6
    when "Chiapas"
      @zona = 6
    when "Chihuahua (Juárez)"
      @zona = 1
    when "Chihuahua (Manuel Ojinaga)"
      @zona = 1
    when "Chihuahua (OTRAS CIUDADES)"
      @zona = 3
    when "Coahuila (Ciudad Acuña)"
      @zona = 1
    when "Coahuila (Matamoros)"
      @zona = 1
    when "Coahuila (Piedras Negras)"
      @zona = 1
    when "Coahuila (OTRAS CIUDADES)"
      @zona = 3
    when "Colima"
      @zona = 4
    when "Distrito Federal(y área metropolitana)"
      @zona = 1
    when "Durango"
      @zona = 4
    when "Estado de México(excepto área metropolitana)"
      @zona = 3
    when "Guanajuato"
      @zona = 4
    when "Guerrero(Acapulco)"
      @zona = 4
    when "Guerrero(Zihuatanejo)"
      @zona = 4
    when "Guerrero(OTRAS CIUDADES)"
      @zona = 4
    when "Hidalgo"
      @zona = 3
    when "Jalisco"
      @zona = 3
    when "Michoacán"
      @zona = 6
    when "Morelos"
      @zona = 3
    when "Nayarit"
      @zona = 4
    when "Nuevo León (Anáhuac)"
      @zona = 1
    when "Nuevo León (OTRAS CIUDADES)"
      @zona = 2
    when "Oaxaca"
      @zona = 6
    when "Puebla"
      @zona = 3
    when "Querétaro"
      @zona = 4
    when "Quintana Roo"
      @zona = 4
    when "San Luis Potosí"
      @zona = 5
    when "Sinaloa"
      @zona = 4
    when "Sonora (Agua Prieta)"
      @zona = 1
    when "Sonora (Caborca)"
      @zona = 1
    when "Sonora (Ciudad de Cananea)"
      @zona = 1
    when "Sonora (Nogales)"
      @zona = 1
    when "Sonora (Puerto Peñasco)"
      @zona = 1
    when "Sonora (San Luis Río Colorado)"
      @zona = 1
    when "Sonora (Sonoita)"
      @zona = 1
    when "Sonora (OTRAS CIUDADES)"
      @zona = 2
    when "Tabasco"
      @zona = 6
    when "Tamaulipas (Ciudad Camargo)"
      @zona = 1
    when "Tamaulipas (Ciudad Gustavo Díaz Ordaz)"
      @zona = 1
    when "Tamaulipas (Ciudad Miguel Alemán)"
      @zona = 1
    when "Tamaulipas (Ciudad Río Bravo)"
      @zona = 1
    when "Tamaulipas (Nueva Ciudad Guerrero)"
      @zona = 1
    when "Tamaulipas (Nuevo Laredo)"
      @zona = 1
    when "Tamaulipas (Reynosa)"
      @zona = 1
    when "Tamaulipas (Valle Hermoso)"
      @zona = 1
    when "Tamaulipas (OTRAS CIUDADES)"
      @zona = 3
    when "Tlaxcala"
      @zona = 5
    when "Veracruz"
      @zona = 5
    when "Yucatán"
      @zona = 6
    when "Zacatecas" 
      @zona = 5
      
    else
      @zona = 0
    end

  
    @calculo_deducible1 = (((LookupTablePrecio.where(zona: @zona, edad: @layout.edadetitular, producto: @prod).first.pan_mx_i)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: @prod).first.pan_mx_i)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelamadredeltitular, producto: @prod).first.pan_mx_i)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegradeltitular, producto: @prod).first.pan_mx_i)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelpadredeltitular, producto: @prod).first.pan_mx_i)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegrodeltitular, producto: @prod).first.pan_mx_i) + (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_i)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: @prod).first.pan_mx_i)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: @prod).first.pan_mx_i)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_i)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.numdehijosconedadde24ymenores, producto: @prod).first.pan_mx_i))*(1+@rpf)/(@formapago.to_f)+900)*(1+@iva)
    @calculo_deducible2 = (((LookupTablePrecio.where(zona: @zona, edad: @layout.edadetitular, producto: @prod).first.pan_mx_ii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: @prod).first.pan_mx_ii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelamadredeltitular, producto: @prod).first.pan_mx_ii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegradeltitular, producto: @prod).first.pan_mx_ii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelpadredeltitular, producto: @prod).first.pan_mx_ii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegrodeltitular, producto: @prod).first.pan_mx_ii) + (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_ii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: @prod).first.pan_mx_ii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: @prod).first.pan_mx_ii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_ii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.numdehijosconedadde24ymenores, producto: @prod).first.pan_mx_ii))*(1+@rpf)/(@formapago.to_f)+900)*(1+@iva)
    @calculo_deducible3 = (((LookupTablePrecio.where(zona: @zona, edad: @layout.edadetitular, producto: @prod).first.pan_mx_iii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: @prod).first.pan_mx_iii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelamadredeltitular, producto: @prod).first.pan_mx_iii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegradeltitular, producto: @prod).first.pan_mx_iii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelpadredeltitular, producto: @prod).first.pan_mx_iii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegrodeltitular, producto: @prod).first.pan_mx_iii) + (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_iii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: @prod).first.pan_mx_iii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: @prod).first.pan_mx_iii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_iii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.numdehijosconedadde24ymenores, producto: @prod).first.pan_mx_iii))*(1+@rpf)/(@formapago.to_f)+900)*(1+@iva)
    @calculo_deducible4 = (((LookupTablePrecio.where(zona: @zona, edad: @layout.edadetitular, producto: @prod).first.pan_mx_iv)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: @prod).first.pan_mx_iv)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelamadredeltitular, producto: @prod).first.pan_mx_iv)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegradeltitular, producto: @prod).first.pan_mx_iv)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelpadredeltitular, producto: @prod).first.pan_mx_iv)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegrodeltitular, producto: @prod).first.pan_mx_iv) + (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_iv)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: @prod).first.pan_mx_iv)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: @prod).first.pan_mx_iv)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_iv)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.numdehijosconedadde24ymenores, producto: @prod).first.pan_mx_iv))*(1+@rpf)/(@formapago.to_f)+900)*(1+@iva)
    @calculo_deducible5 = (((LookupTablePrecio.where(zona: @zona, edad: @layout.edadetitular, producto: @prod).first.pan_mx_v)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: @prod).first.pan_mx_v)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelamadredeltitular, producto: @prod).first.pan_mx_v)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegradeltitular, producto: @prod).first.pan_mx_v)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelpadredeltitular, producto: @prod).first.pan_mx_v)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegrodeltitular, producto: @prod).first.pan_mx_v) + (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_v)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: @prod).first.pan_mx_v)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: @prod).first.pan_mx_v)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_v)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.numdehijosconedadde24ymenores, producto: @prod).first.pan_mx_v))*(1+@rpf)/(@formapago.to_f)+900)*(1+@iva)
    
    @subsecuentes1 = (((LookupTablePrecio.where(zona: @zona, edad: @layout.edadetitular, producto: @prod).first.pan_mx_i)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: @prod).first.pan_mx_i)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelamadredeltitular, producto: @prod).first.pan_mx_i)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegradeltitular, producto: @prod).first.pan_mx_i)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelpadredeltitular, producto: @prod).first.pan_mx_i)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegrodeltitular, producto: @prod).first.pan_mx_i) + (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_i)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: @prod).first.pan_mx_i)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: @prod).first.pan_mx_i)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_i)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.numdehijosconedadde24ymenores, producto: @prod).first.pan_mx_i))*(1+@rpf)*(@formapagosubsecuentes.to_f)+0)*(1+@iva)
    @subsecuentes2 = (((LookupTablePrecio.where(zona: @zona, edad: @layout.edadetitular, producto: @prod).first.pan_mx_ii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: @prod).first.pan_mx_ii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelamadredeltitular, producto: @prod).first.pan_mx_ii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegradeltitular, producto: @prod).first.pan_mx_ii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelpadredeltitular, producto: @prod).first.pan_mx_ii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegrodeltitular, producto: @prod).first.pan_mx_ii) + (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_ii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: @prod).first.pan_mx_ii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: @prod).first.pan_mx_ii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_ii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.numdehijosconedadde24ymenores, producto: @prod).first.pan_mx_ii))*(1+@rpf)*(@formapagosubsecuentes.to_f)+0)*(1+@iva)
    @subsecuentes3 = (((LookupTablePrecio.where(zona: @zona, edad: @layout.edadetitular, producto: @prod).first.pan_mx_iii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: @prod).first.pan_mx_iii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelamadredeltitular, producto: @prod).first.pan_mx_iii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegradeltitular, producto: @prod).first.pan_mx_iii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelpadredeltitular, producto: @prod).first.pan_mx_iii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegrodeltitular, producto: @prod).first.pan_mx_iii) + (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_iii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: @prod).first.pan_mx_iii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: @prod).first.pan_mx_iii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_iii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.numdehijosconedadde24ymenores, producto: @prod).first.pan_mx_iii))*(1+@rpf)*(@formapagosubsecuentes.to_f)+0)*(1+@iva)
    @subsecuentes4 = (((LookupTablePrecio.where(zona: @zona, edad: @layout.edadetitular, producto: @prod).first.pan_mx_iv)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: @prod).first.pan_mx_iv)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelamadredeltitular, producto: @prod).first.pan_mx_iv)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegradeltitular, producto: @prod).first.pan_mx_iv)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelpadredeltitular, producto: @prod).first.pan_mx_iv)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegrodeltitular, producto: @prod).first.pan_mx_iv) + (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_iv)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: @prod).first.pan_mx_iv)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: @prod).first.pan_mx_iv)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_iv)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.numdehijosconedadde24ymenores, producto: @prod).first.pan_mx_iv))*(1+@rpf)*(@formapagosubsecuentes.to_f)+0)*(1+@iva)
    @subsecuentes5 = (((LookupTablePrecio.where(zona: @zona, edad: @layout.edadetitular, producto: @prod).first.pan_mx_v)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: @prod).first.pan_mx_v)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelamadredeltitular, producto: @prod).first.pan_mx_v)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegradeltitular, producto: @prod).first.pan_mx_v)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelpadredeltitular, producto: @prod).first.pan_mx_v)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegrodeltitular, producto: @prod).first.pan_mx_v) + (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_v)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: @prod).first.pan_mx_v)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: @prod).first.pan_mx_v)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_v)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.numdehijosconedadde24ymenores, producto: @prod).first.pan_mx_v))*(1+@rpf)*(@formapagosubsecuentes.to_f)+0)*(1+@iva)

    @calculo_total1 = @calculo_deducible1 + ((@formapago -1)*@subsecuentes1)
    @calculo_total2 = @calculo_deducible2 + ((@formapago-1)*@subsecuentes2)
    @calculo_total3 = @calculo_deducible3 + ((@formapago-1)*@subsecuentes3)
    @calculo_total4 = @calculo_deducible4 + ((@formapago-1)*@subsecuentes4)
    @calculo_total5 = @calculo_deducible5 + ((@formapago-1)*@subsecuentes5)
  end
  def palig
    @layout = Aglayout.last
    
    if @layout.formadepago == 'Anual'
      @formapago = 1
      @rpf = 0
      @formapagosubsecuentes = 0/@formapago
    elsif @layout.formadepago == 'Semestral'
       @formapago = 2
       @rpf = 0.045
       @formapagosubsecuentes = 0.5
    elsif @layout.formadepago == 'Trimestral'
       @formapago = 4
       @rpf = 0.075
       @formapagosubsecuentes = 0.25
    end
      
    @iva = 0.16
    @prod = @layout.plan
    if @prod == 'PA'
      @plan = Plan.where(nombre: 'Preferred Access').first
    elsif @prod == 'WA'
      @plan = Plan.where(nombre: 'World Access').first
    end
    
    case @layout.estadoderesidencia
    when "Aguascalientes"
      @zona = 5
    when "Baja California (Ensenada)"
      @zona = 1
    when "Baja California (Mexicalli)"
      @zona = 1
    when "Baja California (Playas de Rosarito)"
      @zona = 1
    when "Baja California (Rodolfo Sánchez T.  (Maneadero))"
      @zona = 1
    when "Baja California (San Felipe)"
      @zona = 1
    when "Baja California (Tecate)"
      @zona = 1
    when "Baja California (Tijuana)"
      @zona = 1
    when "Baja California (OTRAS CIUDADES)"
      @zona = 2
    when "Baja California Sur"
      @zona = 2
    when "Campeche"
      @zona = 6
    when "Chiapas"
      @zona = 6
    when "Chihuahua (Juárez)"
      @zona = 1
    when "Chihuahua (Manuel Ojinaga)"
      @zona = 1
    when "Chihuahua (OTRAS CIUDADES)"
      @zona = 3
    when "Coahuila (Ciudad Acuña)"
      @zona = 1
    when "Coahuila (Matamoros)"
      @zona = 1
    when "Coahuila (Piedras Negras)"
      @zona = 1
    when "Coahuila (OTRAS CIUDADES)"
      @zona = 3
    when "Colima"
      @zona = 4
    when "Distrito Federal(y área metropolitana)"
      @zona = 1
    when "Durango"
      @zona = 4
    when "Estado de México(excepto área metropolitana)"
      @zona = 3
    when "Guanajuato"
      @zona = 4
    when "Guerrero(Acapulco)"
      @zona = 4
    when "Guerrero(Zihuatanejo)"
      @zona = 4
    when "Guerrero(OTRAS CIUDADES)"
      @zona = 4
    when "Hidalgo"
      @zona = 3
    when "Jalisco"
      @zona = 3
    when "Michoacán"
      @zona = 6
    when "Morelos"
      @zona = 3
    when "Nayarit"
      @zona = 4
    when "Nuevo León (Anáhuac)"
      @zona = 1
    when "Nuevo León (OTRAS CIUDADES)"
      @zona = 2
    when "Oaxaca"
      @zona = 6
    when "Puebla"
      @zona = 3
    when "Querétaro"
      @zona = 4
    when "Quintana Roo"
      @zona = 4
    when "San Luis Potosí"
      @zona = 5
    when "Sinaloa"
      @zona = 4
    when "Sonora (Agua Prieta)"
      @zona = 1
    when "Sonora (Caborca)"
      @zona = 1
    when "Sonora (Ciudad de Cananea)"
      @zona = 1
    when "Sonora (Nogales)"
      @zona = 1
    when "Sonora (Puerto Peñasco)"
      @zona = 1
    when "Sonora (San Luis Río Colorado)"
      @zona = 1
    when "Sonora (Sonoita)"
      @zona = 1
    when "Sonora (OTRAS CIUDADES)"
      @zona = 2
    when "Tabasco"
      @zona = 6
    when "Tamaulipas (Ciudad Camargo)"
      @zona = 1
    when "Tamaulipas (Ciudad Gustavo Díaz Ordaz)"
      @zona = 1
    when "Tamaulipas (Ciudad Miguel Alemán)"
      @zona = 1
    when "Tamaulipas (Ciudad Río Bravo)"
      @zona = 1
    when "Tamaulipas (Nueva Ciudad Guerrero)"
      @zona = 1
    when "Tamaulipas (Nuevo Laredo)"
      @zona = 1
    when "Tamaulipas (Reynosa)"
      @zona = 1
    when "Tamaulipas (Valle Hermoso)"
      @zona = 1
    when "Tamaulipas (OTRAS CIUDADES)"
      @zona = 3
    when "Tlaxcala"
      @zona = 5
    when "Veracruz"
      @zona = 5
    when "Yucatán"
      @zona = 6
    when "Zacatecas" 
      @zona = 5
      
    else
      @zona = 0
    end

  
    @calculo_deducible1 = (((LookupTablePrecio.where(zona: @zona, edad: @layout.edadetitular, producto: @prod).first.pan_mx_i)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: @prod).first.pan_mx_i)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelamadredeltitular, producto: @prod).first.pan_mx_i)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegradeltitular, producto: @prod).first.pan_mx_i)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelpadredeltitular, producto: @prod).first.pan_mx_i)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegrodeltitular, producto: @prod).first.pan_mx_i) + (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_i)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: @prod).first.pan_mx_i)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: @prod).first.pan_mx_i)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_i)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.numdehijosconedadde24ymenores, producto: @prod).first.pan_mx_i))*(1+@rpf)/(@formapago.to_f)+900)*(1+@iva)
    @calculo_deducible2 = (((LookupTablePrecio.where(zona: @zona, edad: @layout.edadetitular, producto: @prod).first.pan_mx_ii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: @prod).first.pan_mx_ii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelamadredeltitular, producto: @prod).first.pan_mx_ii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegradeltitular, producto: @prod).first.pan_mx_ii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelpadredeltitular, producto: @prod).first.pan_mx_ii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegrodeltitular, producto: @prod).first.pan_mx_ii) + (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_ii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: @prod).first.pan_mx_ii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: @prod).first.pan_mx_ii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_ii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.numdehijosconedadde24ymenores, producto: @prod).first.pan_mx_ii))*(1+@rpf)/(@formapago.to_f)+900)*(1+@iva)
    @calculo_deducible3 = (((LookupTablePrecio.where(zona: @zona, edad: @layout.edadetitular, producto: @prod).first.pan_mx_iii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: @prod).first.pan_mx_iii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelamadredeltitular, producto: @prod).first.pan_mx_iii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegradeltitular, producto: @prod).first.pan_mx_iii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelpadredeltitular, producto: @prod).first.pan_mx_iii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegrodeltitular, producto: @prod).first.pan_mx_iii) + (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_iii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: @prod).first.pan_mx_iii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: @prod).first.pan_mx_iii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_iii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.numdehijosconedadde24ymenores, producto: @prod).first.pan_mx_iii))*(1+@rpf)/(@formapago.to_f)+900)*(1+@iva)
    @calculo_deducible4 = (((LookupTablePrecio.where(zona: @zona, edad: @layout.edadetitular, producto: @prod).first.pan_mx_iv)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: @prod).first.pan_mx_iv)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelamadredeltitular, producto: @prod).first.pan_mx_iv)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegradeltitular, producto: @prod).first.pan_mx_iv)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelpadredeltitular, producto: @prod).first.pan_mx_iv)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegrodeltitular, producto: @prod).first.pan_mx_iv) + (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_iv)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: @prod).first.pan_mx_iv)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: @prod).first.pan_mx_iv)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_iv)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.numdehijosconedadde24ymenores, producto: @prod).first.pan_mx_iv))*(1+@rpf)/(@formapago.to_f)+900)*(1+@iva)
    @calculo_deducible5 = (((LookupTablePrecio.where(zona: @zona, edad: @layout.edadetitular, producto: @prod).first.pan_mx_v)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: @prod).first.pan_mx_v)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelamadredeltitular, producto: @prod).first.pan_mx_v)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegradeltitular, producto: @prod).first.pan_mx_v)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelpadredeltitular, producto: @prod).first.pan_mx_v)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegrodeltitular, producto: @prod).first.pan_mx_v) + (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_v)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: @prod).first.pan_mx_v)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: @prod).first.pan_mx_v)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_v)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.numdehijosconedadde24ymenores, producto: @prod).first.pan_mx_v))*(1+@rpf)/(@formapago.to_f)+900)*(1+@iva)
    
    @subsecuentes1 = (((LookupTablePrecio.where(zona: @zona, edad: @layout.edadetitular, producto: @prod).first.pan_mx_i)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: @prod).first.pan_mx_i)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelamadredeltitular, producto: @prod).first.pan_mx_i)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegradeltitular, producto: @prod).first.pan_mx_i)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelpadredeltitular, producto: @prod).first.pan_mx_i)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegrodeltitular, producto: @prod).first.pan_mx_i) + (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_i)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: @prod).first.pan_mx_i)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: @prod).first.pan_mx_i)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_i)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.numdehijosconedadde24ymenores, producto: @prod).first.pan_mx_i))*(1+@rpf)*(@formapagosubsecuentes.to_f)+0)*(1+@iva)
    @subsecuentes2 = (((LookupTablePrecio.where(zona: @zona, edad: @layout.edadetitular, producto: @prod).first.pan_mx_ii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: @prod).first.pan_mx_ii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelamadredeltitular, producto: @prod).first.pan_mx_ii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegradeltitular, producto: @prod).first.pan_mx_ii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelpadredeltitular, producto: @prod).first.pan_mx_ii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegrodeltitular, producto: @prod).first.pan_mx_ii) + (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_ii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: @prod).first.pan_mx_ii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: @prod).first.pan_mx_ii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_ii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.numdehijosconedadde24ymenores, producto: @prod).first.pan_mx_ii))*(1+@rpf)*(@formapagosubsecuentes.to_f)+0)*(1+@iva)
    @subsecuentes3 = (((LookupTablePrecio.where(zona: @zona, edad: @layout.edadetitular, producto: @prod).first.pan_mx_iii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: @prod).first.pan_mx_iii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelamadredeltitular, producto: @prod).first.pan_mx_iii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegradeltitular, producto: @prod).first.pan_mx_iii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelpadredeltitular, producto: @prod).first.pan_mx_iii)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegrodeltitular, producto: @prod).first.pan_mx_iii) + (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_iii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: @prod).first.pan_mx_iii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: @prod).first.pan_mx_iii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_iii)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.numdehijosconedadde24ymenores, producto: @prod).first.pan_mx_iii))*(1+@rpf)*(@formapagosubsecuentes.to_f)+0)*(1+@iva)
    @subsecuentes4 = (((LookupTablePrecio.where(zona: @zona, edad: @layout.edadetitular, producto: @prod).first.pan_mx_iv)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: @prod).first.pan_mx_iv)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelamadredeltitular, producto: @prod).first.pan_mx_iv)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegradeltitular, producto: @prod).first.pan_mx_iv)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelpadredeltitular, producto: @prod).first.pan_mx_iv)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegrodeltitular, producto: @prod).first.pan_mx_iv) + (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_iv)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: @prod).first.pan_mx_iv)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: @prod).first.pan_mx_iv)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_iv)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.numdehijosconedadde24ymenores, producto: @prod).first.pan_mx_iv))*(1+@rpf)*(@formapagosubsecuentes.to_f)+0)*(1+@iva)
    @subsecuentes5 = (((LookupTablePrecio.where(zona: @zona, edad: @layout.edadetitular, producto: @prod).first.pan_mx_v)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: @prod).first.pan_mx_v)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelamadredeltitular, producto: @prod).first.pan_mx_v)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegradeltitular, producto: @prod).first.pan_mx_v)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelpadredeltitular, producto: @prod).first.pan_mx_v)+(LookupTablePrecio.where(zona: @zona, edad: @layout.edaddelasuegrodeltitular, producto: @prod).first.pan_mx_v) + (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_v)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: @prod).first.pan_mx_v)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: @prod).first.pan_mx_v)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: @prod).first.pan_mx_v)+ (LookupTablePrecio.where(zona: @zona, edad: @layout.numdehijosconedadde24ymenores, producto: @prod).first.pan_mx_v))*(1+@rpf)*(@formapagosubsecuentes.to_f)+0)*(1+@iva)

    @calculo_total1 = @calculo_deducible1 + ((@formapago -1)*@subsecuentes1)
    @calculo_total2 = @calculo_deducible2 + ((@formapago-1)*@subsecuentes2)
    @calculo_total3 = @calculo_deducible3 + ((@formapago-1)*@subsecuentes3)
    @calculo_total4 = @calculo_deducible4 + ((@formapago-1)*@subsecuentes4)
    @calculo_total5 = @calculo_deducible5 + ((@formapago-1)*@subsecuentes5)
  end

def bupa
  @layout = Aglayout.last
  @derechopoliza = 937.50
  @iva = 0.16
  if @layout.numdehijosconedadde24ymenores == '1'
    @menores = '1 menor'


  elsif @layout.numdehijosconedadde24ymenores == '2'
    @menores = '2 menores'
  elsif @layout.numdehijosconedadde24ymenores =='3' || @layout.numdehijosconedadde24ymenores =='4' || @layout.numdehijosconedadde24ymenores =='5' || @layout.numdehijosconedadde24ymenores =='6' || @layout.numdehijosconedadde24ymenores =='7' || @layout.numdehijosconedadde24ymenores =='8'
    @menores ='3 ó más menores'
  else
    @menores = ''
  end
  if @layout.formadepago == 'Anual'
      @formapago = 1
      @rpf = 0
      @formapagosubsecuentes = 0/@formapago
    elsif @layout.formadepago == 'Semestral'
       @formapago = 2
       @rpf = 0.045
       @formapagosubsecuentes = 0.5
    elsif @layout.formadepago == 'Trimestral'
       @formapago = 4
       @rpf = 0.075
       @formapagosubsecuentes = 0.25
    end

     case @layout.estadoderesidencia
    when "Aguascalientes"
      @zona = 4
    when "Baja California (Ensenada)"
      @zona = 1
    when "Baja California (Mexicalli)"
      @zona = 1
    when "Baja California (Playas de Rosarito)"
      @zona = 1
    when "Baja California (Rodolfo Sánchez T.  (Maneadero))"
      @zona = 1
    when "Baja California (San Felipe)"
      @zona = 1
    when "Baja California (Tecate)"
      @zona = 1
    when "Baja California (Tijuana)"
      @zona = 1
    when "Baja California (OTRAS CIUDADES)"
      @zona = 2
    when "Baja California Sur"
      @zona = 2
    when "Campeche"
      @zona = 5
    when "Chiapas"
      @zona = 5
    when "Chihuahua (Juárez)"
      @zona = 1
    when "Chihuahua (Manuel Ojinaga)"
      @zona = 1
    when "Chihuahua (OTRAS CIUDADES)"
      @zona = 2
    when "Coahuila (Ciudad Acuña)"
      @zona = 1
    when "Coahuila (Matamoros)"
      @zona = 1
    when "Coahuila (Piedras Negras)"
      @zona = 1
    when "Coahuila (OTRAS CIUDADES)"
      @zona = 2
    when "Colima"
      @zona = 4
    when "Distrito Federal(y área metropolitana)"
      @zona = 1
    when "Durango"
      @zona = 4
    when "Estado de México(excepto área metropolitana)"
      @zona = 3
    when "Guanajuato"
      @zona = 4
    when "Guerrero(Acapulco)"
      @zona = 3
    when "Guerrero(Zihuatanejo)"
      @zona = 3
    when "Guerrero(OTRAS CIUDADES)"
      @zona = 5
    when "Hidalgo"
      @zona = 3
    when "Jalisco"
      @zona = 3
    when "Michoacán"
      @zona = 4
    when "Morelos"
      @zona = 3
    when "Nayarit"
      @zona = 4
    when "Nuevo León (Anáhuac)"
      @zona = 1
    when "Nuevo León (OTRAS CIUDADES)"
      @zona = 2
    when "Oaxaca"
      @zona = 5
    when "Puebla"
      @zona = 3
    when "Querétaro"
      @zona = 3
    when "Quintana Roo"
      @zona = 3
    when "San Luis Potosí"
      @zona = 4
    when "Sinaloa"
      @zona = 4
    when "Sonora (Agua Prieta)"
      @zona = 1
    when "Sonora (Caborca)"
      @zona = 1
    when "Sonora (Ciudad de Cananea)"
      @zona = 1
    when "Sonora (Nogales)"
      @zona = 1
    when "Sonora (Puerto Peñasco)"
      @zona = 1
    when "Sonora (San Luis Río Colorado)"
      @zona = 1
    when "Sonora (Sonoita)"
      @zona = 1
    when "Sonora (OTRAS CIUDADES)"
      @zona = 2
    when "Tabasco"
      @zona = 5
    when "Tamaulipas (Ciudad Camargo)"
      @zona = 1
    when "Tamaulipas (Ciudad Gustavo Díaz Ordaz)"
      @zona = 1
    when "Tamaulipas (Ciudad Miguel Alemán)"
      @zona = 1
    when "Tamaulipas (Ciudad Río Bravo)"
      @zona = 1
    when "Tamaulipas (Nueva Ciudad Guerrero)"
      @zona = 1
    when "Tamaulipas (Nuevo Laredo)"
      @zona = 1
    when "Tamaulipas (Reynosa)"
      @zona = 1
    when "Tamaulipas (Valle Hermoso)"
      @zona = 1
    when "Tamaulipas (OTRAS CIUDADES)"
      @zona = 2
    when "Tlaxcala"
      @zona = 5
    when "Veracruz"
      @zona = 5
    when "Yucatán"
      @zona = 5
    when "Zacatecas" 
      @zona = 4
      
    else
      @zona = 0
    end

    @titulardiamondplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Diamond').first.plan1)
    @titulardiamondplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Diamond').first.plan2)
    @titulardiamondplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Diamond').first.plan3)
    @titulardiamondplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Diamond').first.plan4)
    @titulardiamondplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Diamond').first.plan5)

    if @layout.edaddeconyugue != ""
      @conyuguediamondplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Diamond').first.plan1)
      @conyuguediamondplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Diamond').first.plan2)
      @conyuguediamondplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Diamond').first.plan3)
      @conyuguediamondplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Diamond').first.plan4)
      @conyuguediamondplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Diamond').first.plan5)
    else
      @conyuguediamondplan1 = 0
      @conyuguediamondplan2 = 0
      @conyuguediamondplan3 = 0
      @conyuguediamondplan4 = 0
      @conyuguediamondplan5 = 0
    end

    if @layout.numdehijosconedadde24ymenores != ""
      @hijosmenoresdiamondplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Diamond').first.plan1)
      @hijosmenoresdiamondplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Diamond').first.plan2)
      @hijosmenoresdiamondplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Diamond').first.plan3)
      @hijosmenoresdiamondplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Diamond').first.plan4)
      @hijosmenoresdiamondplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Diamond').first.plan5)
    else
      @hijosmenoresdiamondplan1 = 0
      @hijosmenoresdiamondplan2 = 0
      @hijosmenoresdiamondplan3 = 0
      @hijosmenoresdiamondplan4 = 0
      @hijosmenoresdiamondplan5 = 0
    end

    if @layout.edaddehijo1mayora24 != ''
      @hijo1diamondplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Diamond').first.plan1)
      @hijo1diamondplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Diamond').first.plan2)
      @hijo1diamondplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Diamond').first.plan3)
      @hijo1diamondplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Diamond').first.plan4)
      @hijo1diamondplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Diamond').first.plan5)
      
    else

      @hijo1diamondplan1 = 0
      @hijo1diamondplan2 = 0
      @hijo1diamondplan3 = 0
      @hijo1diamondplan4 = 0
      @hijo1diamondplan5 = 0
    end

    if @layout.edaddehijo2mayora24 != ''
      @hijo2diamondplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Diamond').first.plan1)
      @hijo2diamondplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Diamond').first.plan2)
      @hijo2diamondplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Diamond').first.plan3)
      @hijo2diamondplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Diamond').first.plan4)
      @hijo2diamondplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Diamond').first.plan5)

    else
      @hijo2diamondplan1 = 0
      @hijo2diamondplan2 = 0
      @hijo2diamondplan3 = 0
      @hijo2diamondplan4 = 0
      @hijo2diamondplan5 = 0
    end

    if @layout.edaddehijo3mayora24 != ''
      @hijo3diamondplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Diamond').first.plan1)
      @hijo3diamondplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Diamond').first.plan2)
      @hijo3diamondplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Diamond').first.plan3)
      @hijo3diamondplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Diamond').first.plan4)
      @hijo3diamondplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Diamond').first.plan5)
  
    else

      @hijo3diamondplan1 = 0
      @hijo3diamondplan2 = 0
      @hijo3diamondplan3 = 0
      @hijo3diamondplan4 = 0
      @hijo3diamondplan5 = 0
    end

    if @layout.edaddehijo4mayora24 != ''
      @hijo4diamondplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Diamond').first.plan1)
      @hijo4diamondplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Diamond').first.plan2)
      @hijo4diamondplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Diamond').first.plan3)
      @hijo4diamondplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Diamond').first.plan4)
      @hijo4diamondplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Diamond').first.plan5)
     
    else

      @hijo4diamondplan1 = 0
      @hijo4diamondplan2 = 0
      @hijo4diamondplan3 = 0
      @hijo4diamondplan4 = 0
      @hijo4diamondplan5 = 0
    end

    @dependientesdiamondplan1 = @hijosmenoresdiamondplan1 + @hijo1diamondplan1 + @hijo2diamondplan1 + @hijo3diamondplan1 + @hijo4diamondplan1
    @dependientesdiamondplan2 = @hijosmenoresdiamondplan2 + @hijo1diamondplan2 + @hijo2diamondplan2 + @hijo3diamondplan2 + @hijo4diamondplan2
    @dependientesdiamondplan3 = @hijosmenoresdiamondplan3 + @hijo1diamondplan3 + @hijo2diamondplan3 + @hijo3diamondplan3 + @hijo4diamondplan3
    @dependientesdiamondplan4 = @hijosmenoresdiamondplan4 + @hijo1diamondplan4 + @hijo2diamondplan4 + @hijo3diamondplan4 + @hijo4diamondplan4
    @dependientesdiamondplan5 = @hijosmenoresdiamondplan5 + @hijo1diamondplan5 + @hijo2diamondplan5 + @hijo3diamondplan5 + @hijo4diamondplan5

    @cobrosadicionalesdiamondplan1 = 0
    @cobrosadicionalesdiamondplan2 = 0
    @cobrosadicionalesdiamondplan3 = 0
    @cobrosadicionalesdiamondplan4 = 0
    @cobrosadicionalesdiamondplan5 = 0

    @tarifatotaldiamondplan1 = @titulardiamondplan1 + @conyuguediamondplan1 + @dependientesdiamondplan1 + @cobrosadicionalesdiamondplan1
    @tarifatotaldiamondplan2 = @titulardiamondplan2 + @conyuguediamondplan2 + @dependientesdiamondplan2 + @cobrosadicionalesdiamondplan2
    @tarifatotaldiamondplan3 = @titulardiamondplan3 + @conyuguediamondplan3 + @dependientesdiamondplan3 + @cobrosadicionalesdiamondplan3
    @tarifatotaldiamondplan4 = @titulardiamondplan4 + @conyuguediamondplan4 + @dependientesdiamondplan4 + @cobrosadicionalesdiamondplan4
    @tarifatotaldiamondplan5 = @titulardiamondplan5 + @conyuguediamondplan5 + @dependientesdiamondplan5 + @cobrosadicionalesdiamondplan5

    @primanetadiamondplan1 = @tarifatotaldiamondplan1 + @rpf + @derechopoliza
    @primanetadiamondplan2 = @tarifatotaldiamondplan2 + @rpf + @derechopoliza
    @primanetadiamondplan3 = @tarifatotaldiamondplan3 + @rpf + @derechopoliza
    @primanetadiamondplan4 = @tarifatotaldiamondplan4 + @rpf + @derechopoliza
    @primanetadiamondplan5 = @tarifatotaldiamondplan5 + @rpf + @derechopoliza

    @ivadiamondplan1 = @primanetadiamondplan1 * @iva
    @ivadiamondplan2 = @primanetadiamondplan2 * @iva
    @ivadiamondplan3 = @primanetadiamondplan3 * @iva
    @ivadiamondplan4 = @primanetadiamondplan4 * @iva
    @ivadiamondplan5 = @primanetadiamondplan5 * @iva

    @primaanualdiamondplan1 = @primanetadiamondplan1 + @ivadiamondplan1
    @primaanualdiamondplan2 = @primanetadiamondplan2 + @ivadiamondplan2
    @primaanualdiamondplan3 = @primanetadiamondplan3 + @ivadiamondplan3
    @primaanualdiamondplan4 = @primanetadiamondplan4 + @ivadiamondplan4
    @primaanualdiamondplan5 = @primanetadiamondplan5 + @ivadiamondplan5

    @primerrecibodiamondplan1 = (@tarifatotaldiamondplan1 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerrecibodiamondplan2 = (@tarifatotaldiamondplan2 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerrecibodiamondplan3 = (@tarifatotaldiamondplan3 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerrecibodiamondplan4 = (@tarifatotaldiamondplan4 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerrecibodiamondplan5 = (@tarifatotaldiamondplan5 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)

    if @formapago != 1
      @recibossubsequentesdiamondplan1 = (@tarifatotaldiamondplan1 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentesdiamondplan2 = (@tarifatotaldiamondplan2 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentesdiamondplan3 = (@tarifatotaldiamondplan3 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentesdiamondplan4 = (@tarifatotaldiamondplan4 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentesdiamondplan5 = (@tarifatotaldiamondplan5 / (@formapago)*(1+@rpf))*(1+@iva)
    else
      @recibossubsequentesdiamondplan1 = 0
      @recibossubsequentesdiamondplan2 = 0
      @recibossubsequentesdiamondplan3 = 0
      @recibossubsequentesdiamondplan4 = 0
      @recibossubsequentesdiamondplan5 = 0
    end

    @titularcompleteplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Complete').first.plan1)
    @titularcompleteplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Complete').first.plan2)
    @titularcompleteplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Complete').first.plan3)
    @titularcompleteplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Complete').first.plan4)
    @titularcompleteplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Complete').first.plan5)

    if @layout.edaddeconyugue != ""
      @conyuguecompleteplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Complete').first.plan1)
      @conyuguecompleteplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Complete').first.plan2)
      @conyuguecompleteplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Complete').first.plan3)
      @conyuguecompleteplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Complete').first.plan4)
      @conyuguecompleteplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Complete').first.plan5)
    else
      @conyuguecompleteplan1 = 0
      @conyuguecompleteplan2 = 0
      @conyuguecompleteplan3 = 0
      @conyuguecompleteplan4 = 0
      @conyuguecompleteplan5 = 0
    end

    if @layout.numdehijosconedadde24ymenores != ""
      @hijosmenorescompleteplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Complete').first.plan1)
      @hijosmenorescompleteplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Complete').first.plan2)
      @hijosmenorescompleteplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Complete').first.plan3)
      @hijosmenorescompleteplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Complete').first.plan4)
      @hijosmenorescompleteplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Complete').first.plan5)
    else
      @hijosmenorescompleteplan1 = 0
      @hijosmenorescompleteplan2 = 0
      @hijosmenorescompleteplan3 = 0
      @hijosmenorescompleteplan4 = 0
      @hijosmenorescompleteplan5 = 0
    end

    if @layout.edaddehijo1mayora24 != ''
      @hijo1completeplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Complete').first.plan1)
      @hijo1completeplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Complete').first.plan2)
      @hijo1completeplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Complete').first.plan3)
      @hijo1completeplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Complete').first.plan4)
      @hijo1completeplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Complete').first.plan5)
      
    else

      @hijo1completeplan1 = 0
      @hijo1completeplan2 = 0
      @hijo1completeplan3 = 0
      @hijo1completeplan4 = 0
      @hijo1completeplan5 = 0
    end

    if @layout.edaddehijo2mayora24 != ''
      @hijo2completeplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Complete').first.plan1)
      @hijo2completeplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Complete').first.plan2)
      @hijo2completeplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Complete').first.plan3)
      @hijo2completeplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Complete').first.plan4)
      @hijo2completeplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Complete').first.plan5)

    else
      @hijo2completeplan1 = 0
      @hijo2completeplan2 = 0
      @hijo2completeplan3 = 0
      @hijo2completeplan4 = 0
      @hijo2completeplan5 = 0
    end

    if @layout.edaddehijo3mayora24 != ''
      @hijo3completeplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Complete').first.plan1)
      @hijo3completeplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Complete').first.plan2)
      @hijo3completeplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Complete').first.plan3)
      @hijo3completeplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Complete').first.plan4)
      @hijo3completeplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Complete').first.plan5)
  
    else

      @hijo3completeplan1 = 0
      @hijo3completeplan2 = 0
      @hijo3completeplan3 = 0
      @hijo3completeplan4 = 0
      @hijo3completeplan5 = 0
    end

    if @layout.edaddehijo4mayora24 != ''
      @hijo4completeplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Complete').first.plan1)
      @hijo4completeplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Complete').first.plan2)
      @hijo4completeplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Complete').first.plan3)
      @hijo4completeplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Complete').first.plan4)
      @hijo4completeplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Complete').first.plan5)
     
    else

      @hijo4completeplan1 = 0
      @hijo4completeplan2 = 0
      @hijo4completeplan3 = 0
      @hijo4completeplan4 = 0
      @hijo4completeplan5 = 0
    end

    @dependientescompleteplan1 = @hijosmenorescompleteplan1 + @hijo1completeplan1 + @hijo2completeplan1 + @hijo3completeplan1 + @hijo4completeplan1
    @dependientescompleteplan2 = @hijosmenorescompleteplan2 + @hijo1completeplan2 + @hijo2completeplan2 + @hijo3completeplan2 + @hijo4completeplan2
    @dependientescompleteplan3 = @hijosmenorescompleteplan3 + @hijo1completeplan3 + @hijo2completeplan3 + @hijo3completeplan3 + @hijo4completeplan3
    @dependientescompleteplan4 = @hijosmenorescompleteplan4 + @hijo1completeplan4 + @hijo2completeplan4 + @hijo3completeplan4 + @hijo4completeplan4
    @dependientescompleteplan5 = @hijosmenorescompleteplan5 + @hijo1completeplan5 + @hijo2completeplan5 + @hijo3completeplan5 + @hijo4completeplan5

    @cobrosadicionalescompleteplan1 = 0
    @cobrosadicionalescompleteplan2 = 0
    @cobrosadicionalescompleteplan3 = 0
    @cobrosadicionalescompleteplan4 = 0
    @cobrosadicionalescompleteplan5 = 0

    @tarifatotalcompleteplan1 = @titularcompleteplan1 + @conyuguecompleteplan1 + @dependientescompleteplan1 + @cobrosadicionalescompleteplan1
    @tarifatotalcompleteplan2 = @titularcompleteplan2 + @conyuguecompleteplan2 + @dependientescompleteplan2 + @cobrosadicionalescompleteplan2
    @tarifatotalcompleteplan3 = @titularcompleteplan3 + @conyuguecompleteplan3 + @dependientescompleteplan3 + @cobrosadicionalescompleteplan3
    @tarifatotalcompleteplan4 = @titularcompleteplan4 + @conyuguecompleteplan4 + @dependientescompleteplan4 + @cobrosadicionalescompleteplan4
    @tarifatotalcompleteplan5 = @titularcompleteplan5 + @conyuguecompleteplan5 + @dependientescompleteplan5 + @cobrosadicionalescompleteplan5

    @primanetacompleteplan1 = @tarifatotalcompleteplan1 + @rpf + @derechopoliza
    @primanetacompleteplan2 = @tarifatotalcompleteplan2 + @rpf + @derechopoliza
    @primanetacompleteplan3 = @tarifatotalcompleteplan3 + @rpf + @derechopoliza
    @primanetacompleteplan4 = @tarifatotalcompleteplan4 + @rpf + @derechopoliza
    @primanetacompleteplan5 = @tarifatotalcompleteplan5 + @rpf + @derechopoliza

    @ivacompleteplan1 = @primanetacompleteplan1 * @iva
    @ivacompleteplan2 = @primanetacompleteplan2 * @iva
    @ivacompleteplan3 = @primanetacompleteplan3 * @iva
    @ivacompleteplan4 = @primanetacompleteplan4 * @iva
    @ivacompleteplan5 = @primanetacompleteplan5 * @iva

    @primaanualcompleteplan1 = @primanetacompleteplan1 + @ivacompleteplan1
    @primaanualcompleteplan2 = @primanetacompleteplan2 + @ivacompleteplan2
    @primaanualcompleteplan3 = @primanetacompleteplan3 + @ivacompleteplan3
    @primaanualcompleteplan4 = @primanetacompleteplan4 + @ivacompleteplan4
    @primaanualcompleteplan5 = @primanetacompleteplan5 + @ivacompleteplan5

    @primerrecibocompleteplan1 = (@tarifatotalcompleteplan1 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerrecibocompleteplan2 = (@tarifatotalcompleteplan2 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerrecibocompleteplan3 = (@tarifatotalcompleteplan3 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerrecibocompleteplan4 = (@tarifatotalcompleteplan4 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerrecibocompleteplan5 = (@tarifatotalcompleteplan5 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)

    if @formapago != 1
      @recibossubsequentescompleteplan1 = (@tarifatotalcompleteplan1 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentescompleteplan2 = (@tarifatotalcompleteplan2 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentescompleteplan3 = (@tarifatotalcompleteplan3 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentescompleteplan4 = (@tarifatotalcompleteplan4 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentescompleteplan5 = (@tarifatotalcompleteplan5 / (@formapago)*(1+@rpf))*(1+@iva)
    else
      @recibossubsequentescompleteplan1 = 0
      @recibossubsequentescompleteplan2 = 0
      @recibossubsequentescompleteplan3 = 0
      @recibossubsequentescompleteplan4 = 0
      @recibossubsequentescompleteplan5 = 0
    end

    @titularadvantageplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Advantage').first.plan1)
    @titularadvantageplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Advantage').first.plan2)
    @titularadvantageplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Advantage').first.plan3)
    @titularadvantageplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Advantage').first.plan4)
    @titularadvantageplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Advantage').first.plan5)

    if @layout.edaddeconyugue != ""
      @conyugueadvantageplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Advantage').first.plan1)
      @conyugueadvantageplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Advantage').first.plan2)
      @conyugueadvantageplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Advantage').first.plan3)
      @conyugueadvantageplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Advantage').first.plan4)
      @conyugueadvantageplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Advantage').first.plan5)
    else
      @conyugueadvantageplan1 = 0
      @conyugueadvantageplan2 = 0
      @conyugueadvantageplan3 = 0
      @conyugueadvantageplan4 = 0
      @conyugueadvantageplan5 = 0
    end

    if @layout.numdehijosconedadde24ymenores != ""
      @hijosmenoresadvantageplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Advantage').first.plan1)
      @hijosmenoresadvantageplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Advantage').first.plan2)
      @hijosmenoresadvantageplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Advantage').first.plan3)
      @hijosmenoresadvantageplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Advantage').first.plan4)
      @hijosmenoresadvantageplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Advantage').first.plan5)
    else
      @hijosmenoresadvantageplan1 = 0
      @hijosmenoresadvantageplan2 = 0
      @hijosmenoresadvantageplan3 = 0
      @hijosmenoresadvantageplan4 = 0
      @hijosmenoresadvantageplan5 = 0
    end

    if @layout.edaddehijo1mayora24 != ''
      @hijo1advantageplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Advantage').first.plan1)
      @hijo1advantageplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Advantage').first.plan2)
      @hijo1advantageplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Advantage').first.plan3)
      @hijo1advantageplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Advantage').first.plan4)
      @hijo1advantageplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Advantage').first.plan5)
      
    else

      @hijo1advantageplan1 = 0
      @hijo1advantageplan2 = 0
      @hijo1advantageplan3 = 0
      @hijo1advantageplan4 = 0
      @hijo1advantageplan5 = 0
    end

    if @layout.edaddehijo2mayora24 != ''
      @hijo2advantageplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Advantage').first.plan1)
      @hijo2advantageplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Advantage').first.plan2)
      @hijo2advantageplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Advantage').first.plan3)
      @hijo2advantageplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Advantage').first.plan4)
      @hijo2advantageplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Advantage').first.plan5)

    else
      @hijo2advantageplan1 = 0
      @hijo2advantageplan2 = 0
      @hijo2advantageplan3 = 0
      @hijo2advantageplan4 = 0
      @hijo2advantageplan5 = 0
    end

    if @layout.edaddehijo3mayora24 != ''
      @hijo3advantageplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Advantage').first.plan1)
      @hijo3advantageplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Advantage').first.plan2)
      @hijo3advantageplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Advantage').first.plan3)
      @hijo3advantageplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Advantage').first.plan4)
      @hijo3advantageplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Advantage').first.plan5)
  
    else

      @hijo3advantageplan1 = 0
      @hijo3advantageplan2 = 0
      @hijo3advantageplan3 = 0
      @hijo3advantageplan4 = 0
      @hijo3advantageplan5 = 0
    end

    if @layout.edaddehijo4mayora24 != ''
      @hijo4advantageplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Advantage').first.plan1)
      @hijo4advantageplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Advantage').first.plan2)
      @hijo4advantageplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Advantage').first.plan3)
      @hijo4advantageplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Advantage').first.plan4)
      @hijo4advantageplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Advantage').first.plan5)
     
    else

      @hijo4advantageplan1 = 0
      @hijo4advantageplan2 = 0
      @hijo4advantageplan3 = 0
      @hijo4advantageplan4 = 0
      @hijo4advantageplan5 = 0
    end

    @dependientesadvantageplan1 = @hijosmenoresadvantageplan1 + @hijo1advantageplan1 + @hijo2advantageplan1 + @hijo3advantageplan1 + @hijo4advantageplan1
    @dependientesadvantageplan2 = @hijosmenoresadvantageplan2 + @hijo1advantageplan2 + @hijo2advantageplan2 + @hijo3advantageplan2 + @hijo4advantageplan2
    @dependientesadvantageplan3 = @hijosmenoresadvantageplan3 + @hijo1advantageplan3 + @hijo2advantageplan3 + @hijo3advantageplan3 + @hijo4advantageplan3
    @dependientesadvantageplan4 = @hijosmenoresadvantageplan4 + @hijo1advantageplan4 + @hijo2advantageplan4 + @hijo3advantageplan4 + @hijo4advantageplan4
    @dependientesadvantageplan5 = @hijosmenoresadvantageplan5 + @hijo1advantageplan5 + @hijo2advantageplan5 + @hijo3advantageplan5 + @hijo4advantageplan5

    @cobrosadicionalesadvantageplan1 = 3525
    @cobrosadicionalesadvantageplan2 = 3525
    @cobrosadicionalesadvantageplan3 = 3525
    @cobrosadicionalesadvantageplan4 = 3525
    @cobrosadicionalesadvantageplan5 = 3525

    @tarifatotaladvantageplan1 = @titularadvantageplan1 + @conyugueadvantageplan1 + @dependientesadvantageplan1 + @cobrosadicionalesadvantageplan1
    @tarifatotaladvantageplan2 = @titularadvantageplan2 + @conyugueadvantageplan2 + @dependientesadvantageplan2 + @cobrosadicionalesadvantageplan2
    @tarifatotaladvantageplan3 = @titularadvantageplan3 + @conyugueadvantageplan3 + @dependientesadvantageplan3 + @cobrosadicionalesadvantageplan3
    @tarifatotaladvantageplan4 = @titularadvantageplan4 + @conyugueadvantageplan4 + @dependientesadvantageplan4 + @cobrosadicionalesadvantageplan4
    @tarifatotaladvantageplan5 = @titularadvantageplan5 + @conyugueadvantageplan5 + @dependientesadvantageplan5 + @cobrosadicionalesadvantageplan5

    @primanetaadvantageplan1 = @tarifatotaladvantageplan1 + @rpf + @derechopoliza
    @primanetaadvantageplan2 = @tarifatotaladvantageplan2 + @rpf + @derechopoliza
    @primanetaadvantageplan3 = @tarifatotaladvantageplan3 + @rpf + @derechopoliza
    @primanetaadvantageplan4 = @tarifatotaladvantageplan4 + @rpf + @derechopoliza
    @primanetaadvantageplan5 = @tarifatotaladvantageplan5 + @rpf + @derechopoliza

    @ivaadvantageplan1 = @primanetaadvantageplan1 * @iva
    @ivaadvantageplan2 = @primanetaadvantageplan2 * @iva
    @ivaadvantageplan3 = @primanetaadvantageplan3 * @iva
    @ivaadvantageplan4 = @primanetaadvantageplan4 * @iva
    @ivaadvantageplan5 = @primanetaadvantageplan5 * @iva

    @primaanualadvantageplan1 = @primanetaadvantageplan1 + @ivaadvantageplan1
    @primaanualadvantageplan2 = @primanetaadvantageplan2 + @ivaadvantageplan2
    @primaanualadvantageplan3 = @primanetaadvantageplan3 + @ivaadvantageplan3
    @primaanualadvantageplan4 = @primanetaadvantageplan4 + @ivaadvantageplan4
    @primaanualadvantageplan5 = @primanetaadvantageplan5 + @ivaadvantageplan5

    @primerreciboadvantageplan1 = (@tarifatotaladvantageplan1 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerreciboadvantageplan2 = (@tarifatotaladvantageplan2 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerreciboadvantageplan3 = (@tarifatotaladvantageplan3 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerreciboadvantageplan4 = (@tarifatotaladvantageplan4 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerreciboadvantageplan5 = (@tarifatotaladvantageplan5 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)

    if @formapago != 1
      @recibossubsequentesadvantageplan1 = (@tarifatotaladvantageplan1 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentesadvantageplan2 = (@tarifatotaladvantageplan2 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentesadvantageplan3 = (@tarifatotaladvantageplan3 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentesadvantageplan4 = (@tarifatotaladvantageplan4 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentesadvantageplan5 = (@tarifatotaladvantageplan5 / (@formapago)*(1+@rpf))*(1+@iva)
    else
      @recibossubsequentesadvantageplan1 = 0
      @recibossubsequentesadvantageplan2 = 0
      @recibossubsequentesadvantageplan3 = 0
      @recibossubsequentesadvantageplan4 = 0
      @recibossubsequentesadvantageplan5 = 0
    end

         @titularsecureplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Secure').first.plan1)
    @titularsecureplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Secure').first.plan2)
    @titularsecureplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Secure').first.plan3)
    @titularsecureplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Secure').first.plan4)
    @titularsecureplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Secure').first.plan5)

    if @layout.edaddeconyugue != ""
      @conyuguesecureplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Secure').first.plan1)
      @conyuguesecureplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Secure').first.plan2)
      @conyuguesecureplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Secure').first.plan3)
      @conyuguesecureplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Secure').first.plan4)
      @conyuguesecureplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Secure').first.plan5)
    else
      @conyuguesecureplan1 = 0
      @conyuguesecureplan2 = 0
      @conyuguesecureplan3 = 0
      @conyuguesecureplan4 = 0
      @conyuguesecureplan5 = 0
    end

    if @layout.numdehijosconedadde24ymenores != ""
      @hijosmenoressecureplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Secure').first.plan1)
      @hijosmenoressecureplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Secure').first.plan2)
      @hijosmenoressecureplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Secure').first.plan3)
      @hijosmenoressecureplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Secure').first.plan4)
      @hijosmenoressecureplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Secure').first.plan5)
    else
      @hijosmenoressecureplan1 = 0
      @hijosmenoressecureplan2 = 0
      @hijosmenoressecureplan3 = 0
      @hijosmenoressecureplan4 = 0
      @hijosmenoressecureplan5 = 0
    end

    if @layout.edaddehijo1mayora24 != ''
      @hijo1secureplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Secure').first.plan1)
      @hijo1secureplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Secure').first.plan2)
      @hijo1secureplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Secure').first.plan3)
      @hijo1secureplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Secure').first.plan4)
      @hijo1secureplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Secure').first.plan5)
      
    else

      @hijo1secureplan1 = 0
      @hijo1secureplan2 = 0
      @hijo1secureplan3 = 0
      @hijo1secureplan4 = 0
      @hijo1secureplan5 = 0
    end

    if @layout.edaddehijo2mayora24 != ''
      @hijo2secureplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Secure').first.plan1)
      @hijo2secureplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Secure').first.plan2)
      @hijo2secureplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Secure').first.plan3)
      @hijo2secureplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Secure').first.plan4)
      @hijo2secureplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Secure').first.plan5)

    else
      @hijo2secureplan1 = 0
      @hijo2secureplan2 = 0
      @hijo2secureplan3 = 0
      @hijo2secureplan4 = 0
      @hijo2secureplan5 = 0
    end

    if @layout.edaddehijo3mayora24 != ''
      @hijo3secureplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Secure').first.plan1)
      @hijo3secureplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Secure').first.plan2)
      @hijo3secureplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Secure').first.plan3)
      @hijo3secureplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Secure').first.plan4)
      @hijo3secureplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Secure').first.plan5)
  
    else

      @hijo3secureplan1 = 0
      @hijo3secureplan2 = 0
      @hijo3secureplan3 = 0
      @hijo3secureplan4 = 0
      @hijo3secureplan5 = 0
    end

    if @layout.edaddehijo4mayora24 != ''
      @hijo4secureplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Secure').first.plan1)
      @hijo4secureplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Secure').first.plan2)
      @hijo4secureplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Secure').first.plan3)
      @hijo4secureplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Secure').first.plan4)
      @hijo4secureplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Secure').first.plan5)
     
    else

      @hijo4secureplan1 = 0
      @hijo4secureplan2 = 0
      @hijo4secureplan3 = 0
      @hijo4secureplan4 = 0
      @hijo4secureplan5 = 0
    end

    @dependientessecureplan1 = @hijosmenoressecureplan1 + @hijo1secureplan1 + @hijo2secureplan1 + @hijo3secureplan1 + @hijo4secureplan1
    @dependientessecureplan2 = @hijosmenoressecureplan2 + @hijo1secureplan2 + @hijo2secureplan2 + @hijo3secureplan2 + @hijo4secureplan2
    @dependientessecureplan3 = @hijosmenoressecureplan3 + @hijo1secureplan3 + @hijo2secureplan3 + @hijo3secureplan3 + @hijo4secureplan3
    @dependientessecureplan4 = @hijosmenoressecureplan4 + @hijo1secureplan4 + @hijo2secureplan4 + @hijo3secureplan4 + @hijo4secureplan4
    @dependientessecureplan5 = @hijosmenoressecureplan5 + @hijo1secureplan5 + @hijo2secureplan5 + @hijo3secureplan5 + @hijo4secureplan5

    @cobrosadicionalessecureplan1 = 3525
    @cobrosadicionalessecureplan2 = 3525
    @cobrosadicionalessecureplan3 = 3525
    @cobrosadicionalessecureplan4 = 3525
    @cobrosadicionalessecureplan5 = 3525

    @tarifatotalsecureplan1 = @titularsecureplan1 + @conyuguesecureplan1 + @dependientessecureplan1 + @cobrosadicionalessecureplan1
    @tarifatotalsecureplan2 = @titularsecureplan2 + @conyuguesecureplan2 + @dependientessecureplan2 + @cobrosadicionalessecureplan2
    @tarifatotalsecureplan3 = @titularsecureplan3 + @conyuguesecureplan3 + @dependientessecureplan3 + @cobrosadicionalessecureplan3
    @tarifatotalsecureplan4 = @titularsecureplan4 + @conyuguesecureplan4 + @dependientessecureplan4 + @cobrosadicionalessecureplan4
    @tarifatotalsecureplan5 = @titularsecureplan5 + @conyuguesecureplan5 + @dependientessecureplan5 + @cobrosadicionalessecureplan5

    @primanetasecureplan1 = @tarifatotalsecureplan1 + @rpf + @derechopoliza
    @primanetasecureplan2 = @tarifatotalsecureplan2 + @rpf + @derechopoliza
    @primanetasecureplan3 = @tarifatotalsecureplan3 + @rpf + @derechopoliza
    @primanetasecureplan4 = @tarifatotalsecureplan4 + @rpf + @derechopoliza
    @primanetasecureplan5 = @tarifatotalsecureplan5 + @rpf + @derechopoliza

    @ivasecureplan1 = @primanetasecureplan1 * @iva
    @ivasecureplan2 = @primanetasecureplan2 * @iva
    @ivasecureplan3 = @primanetasecureplan3 * @iva
    @ivasecureplan4 = @primanetasecureplan4 * @iva
    @ivasecureplan5 = @primanetasecureplan5 * @iva

    @primaanualsecureplan1 = @primanetasecureplan1 + @ivasecureplan1
    @primaanualsecureplan2 = @primanetasecureplan2 + @ivasecureplan2
    @primaanualsecureplan3 = @primanetasecureplan3 + @ivasecureplan3
    @primaanualsecureplan4 = @primanetasecureplan4 + @ivasecureplan4
    @primaanualsecureplan5 = @primanetasecureplan5 + @ivasecureplan5

    @primerrecibosecureplan1 = (@tarifatotalsecureplan1 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerrecibosecureplan2 = (@tarifatotalsecureplan2 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerrecibosecureplan3 = (@tarifatotalsecureplan3 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerrecibosecureplan4 = (@tarifatotalsecureplan4 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerrecibosecureplan5 = (@tarifatotalsecureplan5 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)

    if @formapago != 1
      @recibossubsequentessecureplan1 = (@tarifatotalsecureplan1 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentessecureplan2 = (@tarifatotalsecureplan2 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentessecureplan3 = (@tarifatotalsecureplan3 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentessecureplan4 = (@tarifatotalsecureplan4 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentessecureplan5 = (@tarifatotalsecureplan5 / (@formapago)*(1+@rpf))*(1+@iva)
    else
      @recibossubsequentessecureplan1 = 0
      @recibossubsequentessecureplan2 = 0
      @recibossubsequentessecureplan3 = 0
      @recibossubsequentessecureplan4 = 0
      @recibossubsequentessecureplan5 = 0
    end

        @titularessentialplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Essential').first.plan1)
    @titularessentialplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Essential').first.plan2)
    @titularessentialplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Essential').first.plan3)
    @titularessentialplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Essential').first.plan4)
    @titularessentialplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Essential').first.plan5)

    if @layout.edaddeconyugue != ""
      @conyugueessentialplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Essential').first.plan1)
      @conyugueessentialplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Essential').first.plan2)
      @conyugueessentialplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Essential').first.plan3)
      @conyugueessentialplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Essential').first.plan4)
      @conyugueessentialplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Essential').first.plan5)
    else
      @conyugueessentialplan1 = 0
      @conyugueessentialplan2 = 0
      @conyugueessentialplan3 = 0
      @conyugueessentialplan4 = 0
      @conyugueessentialplan5 = 0
    end

    if @layout.numdehijosconedadde24ymenores != ""
      @hijosmenoresessentialplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Essential').first.plan1)
      @hijosmenoresessentialplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Essential').first.plan2)
      @hijosmenoresessentialplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Essential').first.plan3)
      @hijosmenoresessentialplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Essential').first.plan4)
      @hijosmenoresessentialplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Essential').first.plan5)
    else
      @hijosmenoresessentialplan1 = 0
      @hijosmenoresessentialplan2 = 0
      @hijosmenoresessentialplan3 = 0
      @hijosmenoresessentialplan4 = 0
      @hijosmenoresessentialplan5 = 0
    end

    if @layout.edaddehijo1mayora24 != ''
      @hijo1essentialplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Essential').first.plan1)
      @hijo1essentialplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Essential').first.plan2)
      @hijo1essentialplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Essential').first.plan3)
      @hijo1essentialplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Essential').first.plan4)
      @hijo1essentialplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Essential').first.plan5)
      
    else

      @hijo1essentialplan1 = 0
      @hijo1essentialplan2 = 0
      @hijo1essentialplan3 = 0
      @hijo1essentialplan4 = 0
      @hijo1essentialplan5 = 0
    end

    if @layout.edaddehijo2mayora24 != ''
      @hijo2essentialplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Essential').first.plan1)
      @hijo2essentialplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Essential').first.plan2)
      @hijo2essentialplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Essential').first.plan3)
      @hijo2essentialplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Essential').first.plan4)
      @hijo2essentialplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Essential').first.plan5)

    else
      @hijo2essentialplan1 = 0
      @hijo2essentialplan2 = 0
      @hijo2essentialplan3 = 0
      @hijo2essentialplan4 = 0
      @hijo2essentialplan5 = 0
    end

    if @layout.edaddehijo3mayora24 != ''
      @hijo3essentialplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Essential').first.plan1)
      @hijo3essentialplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Essential').first.plan2)
      @hijo3essentialplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Essential').first.plan3)
      @hijo3essentialplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Essential').first.plan4)
      @hijo3essentialplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Essential').first.plan5)
  
    else

      @hijo3essentialplan1 = 0
      @hijo3essentialplan2 = 0
      @hijo3essentialplan3 = 0
      @hijo3essentialplan4 = 0
      @hijo3essentialplan5 = 0
    end

    if @layout.edaddehijo4mayora24 != ''
      @hijo4essentialplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Essential').first.plan1)
      @hijo4essentialplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Essential').first.plan2)
      @hijo4essentialplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Essential').first.plan3)
      @hijo4essentialplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Essential').first.plan4)
      @hijo4essentialplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Essential').first.plan5)
     
    else

      @hijo4essentialplan1 = 0
      @hijo4essentialplan2 = 0
      @hijo4essentialplan3 = 0
      @hijo4essentialplan4 = 0
      @hijo4essentialplan5 = 0
    end

    @dependientesessentialplan1 = @hijosmenoresessentialplan1 + @hijo1essentialplan1 + @hijo2essentialplan1 + @hijo3essentialplan1 + @hijo4essentialplan1
    @dependientesessentialplan2 = @hijosmenoresessentialplan2 + @hijo1essentialplan2 + @hijo2essentialplan2 + @hijo3essentialplan2 + @hijo4essentialplan2
    @dependientesessentialplan3 = @hijosmenoresessentialplan3 + @hijo1essentialplan3 + @hijo2essentialplan3 + @hijo3essentialplan3 + @hijo4essentialplan3
    @dependientesessentialplan4 = @hijosmenoresessentialplan4 + @hijo1essentialplan4 + @hijo2essentialplan4 + @hijo3essentialplan4 + @hijo4essentialplan4
    @dependientesessentialplan5 = @hijosmenoresessentialplan5 + @hijo1essentialplan5 + @hijo2essentialplan5 + @hijo3essentialplan5 + @hijo4essentialplan5

    @cobrosadicionalesessentialplan1 = 3525
    @cobrosadicionalesessentialplan2 = 3525
    @cobrosadicionalesessentialplan3 = 3525
    @cobrosadicionalesessentialplan4 = 3525
    @cobrosadicionalesessentialplan5 = 3525

    @tarifatotalessentialplan1 = @titularessentialplan1 + @conyugueessentialplan1 + @dependientesessentialplan1 + @cobrosadicionalesessentialplan1
    @tarifatotalessentialplan2 = @titularessentialplan2 + @conyugueessentialplan2 + @dependientesessentialplan2 + @cobrosadicionalesessentialplan2
    @tarifatotalessentialplan3 = @titularessentialplan3 + @conyugueessentialplan3 + @dependientesessentialplan3 + @cobrosadicionalesessentialplan3
    @tarifatotalessentialplan4 = @titularessentialplan4 + @conyugueessentialplan4 + @dependientesessentialplan4 + @cobrosadicionalesessentialplan4
    @tarifatotalessentialplan5 = @titularessentialplan5 + @conyugueessentialplan5 + @dependientesessentialplan5 + @cobrosadicionalesessentialplan5

    @primanetaessentialplan1 = @tarifatotalessentialplan1 + @rpf + @derechopoliza
    @primanetaessentialplan2 = @tarifatotalessentialplan2 + @rpf + @derechopoliza
    @primanetaessentialplan3 = @tarifatotalessentialplan3 + @rpf + @derechopoliza
    @primanetaessentialplan4 = @tarifatotalessentialplan4 + @rpf + @derechopoliza
    @primanetaessentialplan5 = @tarifatotalessentialplan5 + @rpf + @derechopoliza

    @ivaessentialplan1 = @primanetaessentialplan1 * @iva
    @ivaessentialplan2 = @primanetaessentialplan2 * @iva
    @ivaessentialplan3 = @primanetaessentialplan3 * @iva
    @ivaessentialplan4 = @primanetaessentialplan4 * @iva
    @ivaessentialplan5 = @primanetaessentialplan5 * @iva

    @primaanualessentialplan1 = @primanetaessentialplan1 + @ivaessentialplan1
    @primaanualessentialplan2 = @primanetaessentialplan2 + @ivaessentialplan2
    @primaanualessentialplan3 = @primanetaessentialplan3 + @ivaessentialplan3
    @primaanualessentialplan4 = @primanetaessentialplan4 + @ivaessentialplan4
    @primaanualessentialplan5 = @primanetaessentialplan5 + @ivaessentialplan5

    @primerreciboessentialplan1 = (@tarifatotalessentialplan1 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerreciboessentialplan2 = (@tarifatotalessentialplan2 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerreciboessentialplan3 = (@tarifatotalessentialplan3 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerreciboessentialplan4 = (@tarifatotalessentialplan4 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerreciboessentialplan5 = (@tarifatotalessentialplan5 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)

    if @formapago != 1
      @recibossubsequentesessentialplan1 = (@tarifatotalessentialplan1 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentesessentialplan2 = (@tarifatotalessentialplan2 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentesessentialplan3 = (@tarifatotalessentialplan3 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentesessentialplan4 = (@tarifatotalessentialplan4 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentesessentialplan5 = (@tarifatotalessentialplan5 / (@formapago)*(1+@rpf))*(1+@iva)
    else
      @recibossubsequentesessentialplan1 = 0
      @recibossubsequentesessentialplan2 = 0
      @recibossubsequentesessentialplan3 = 0
      @recibossubsequentesessentialplan4 = 0
      @recibossubsequentesessentialplan5 = 0
    end

        @titulartotalplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Bupa Total').first.plan1)
    @titulartotalplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Bupa Total').first.plan2)
    @titulartotalplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Bupa Total').first.plan3)
    @titulartotalplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Bupa Total').first.plan4)
    @titulartotalplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edadetitular, producto: 'Bupa Total').first.plan5)

    if @layout.edaddeconyugue != ""
      @conyuguetotalplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Bupa Total').first.plan1)
      @conyuguetotalplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Bupa Total').first.plan2)
      @conyuguetotalplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Bupa Total').first.plan3)
      @conyuguetotalplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Bupa Total').first.plan4)
      @conyuguetotalplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddeconyugue, producto: 'Bupa Total').first.plan5)
    else
      @conyuguetotalplan1 = 0
      @conyuguetotalplan2 = 0
      @conyuguetotalplan3 = 0
      @conyuguetotalplan4 = 0
      @conyuguetotalplan5 = 0
    end

    if @layout.numdehijosconedadde24ymenores != ""
      @hijosmenorestotalplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Bupa Total').first.plan1)
      @hijosmenorestotalplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Bupa Total').first.plan2)
      @hijosmenorestotalplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Bupa Total').first.plan3)
      @hijosmenorestotalplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Bupa Total').first.plan4)
      @hijosmenorestotalplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @menores, producto: 'Bupa Total').first.plan5)
    else
      @hijosmenorestotalplan1 = 0
      @hijosmenorestotalplan2 = 0
      @hijosmenorestotalplan3 = 0
      @hijosmenorestotalplan4 = 0
      @hijosmenorestotalplan5 = 0
    end

    if @layout.edaddehijo1mayora24 != ''
      @hijo1totalplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Bupa Total').first.plan1)
      @hijo1totalplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Bupa Total').first.plan2)
      @hijo1totalplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Bupa Total').first.plan3)
      @hijo1totalplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Bupa Total').first.plan4)
      @hijo1totalplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo1mayora24, producto: 'Bupa Total').first.plan5)
      
    else

      @hijo1totalplan1 = 0
      @hijo1totalplan2 = 0
      @hijo1totalplan3 = 0
      @hijo1totalplan4 = 0
      @hijo1totalplan5 = 0
    end

    if @layout.edaddehijo2mayora24 != ''
      @hijo2totalplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Bupa Total').first.plan1)
      @hijo2totalplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Bupa Total').first.plan2)
      @hijo2totalplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Bupa Total').first.plan3)
      @hijo2totalplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Bupa Total').first.plan4)
      @hijo2totalplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo2mayora24, producto: 'Bupa Total').first.plan5)

    else
      @hijo2totalplan1 = 0
      @hijo2totalplan2 = 0
      @hijo2totalplan3 = 0
      @hijo2totalplan4 = 0
      @hijo2totalplan5 = 0
    end

    if @layout.edaddehijo3mayora24 != ''
      @hijo3totalplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Bupa Total').first.plan1)
      @hijo3totalplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Bupa Total').first.plan2)
      @hijo3totalplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Bupa Total').first.plan3)
      @hijo3totalplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Bupa Total').first.plan4)
      @hijo3totalplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo3mayora24, producto: 'Bupa Total').first.plan5)
  
    else

      @hijo3totalplan1 = 0
      @hijo3totalplan2 = 0
      @hijo3totalplan3 = 0
      @hijo3totalplan4 = 0
      @hijo3totalplan5 = 0
    end

    if @layout.edaddehijo4mayora24 != ''
      @hijo4totalplan1 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Bupa Total').first.plan1)
      @hijo4totalplan2 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Bupa Total').first.plan2)
      @hijo4totalplan3 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Bupa Total').first.plan3)
      @hijo4totalplan4 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Bupa Total').first.plan4)
      @hijo4totalplan5 = (BupaLookupPrecio.where(zona: @zona, edad: @layout.edaddehijo4mayora24, producto: 'Bupa Total').first.plan5)
     
    else

      @hijo4totalplan1 = 0
      @hijo4totalplan2 = 0
      @hijo4totalplan3 = 0
      @hijo4totalplan4 = 0
      @hijo4totalplan5 = 0
    end

    @dependientestotalplan1 = @hijosmenorestotalplan1 + @hijo1totalplan1 + @hijo2totalplan1 + @hijo3totalplan1 + @hijo4totalplan1
    @dependientestotalplan2 = @hijosmenorestotalplan2 + @hijo1totalplan2 + @hijo2totalplan2 + @hijo3totalplan2 + @hijo4totalplan2
    @dependientestotalplan3 = @hijosmenorestotalplan3 + @hijo1totalplan3 + @hijo2totalplan3 + @hijo3totalplan3 + @hijo4totalplan3
    @dependientestotalplan4 = @hijosmenorestotalplan4 + @hijo1totalplan4 + @hijo2totalplan4 + @hijo3totalplan4 + @hijo4totalplan4
    @dependientestotalplan5 = @hijosmenorestotalplan5 + @hijo1totalplan5 + @hijo2totalplan5 + @hijo3totalplan5 + @hijo4totalplan5

    @cobrosadicionalestotalplan1 = 3525
    @cobrosadicionalestotalplan2 = 3525
    @cobrosadicionalestotalplan3 = 3525
    @cobrosadicionalestotalplan4 = 3525
    @cobrosadicionalestotalplan5 = 3525

    @tarifatotaltotalplan1 = @titulartotalplan1 + @conyuguetotalplan1 + @dependientestotalplan1 + @cobrosadicionalestotalplan1
    @tarifatotaltotalplan2 = @titulartotalplan2 + @conyuguetotalplan2 + @dependientestotalplan2 + @cobrosadicionalestotalplan2
    @tarifatotaltotalplan3 = @titulartotalplan3 + @conyuguetotalplan3 + @dependientestotalplan3 + @cobrosadicionalestotalplan3
    @tarifatotaltotalplan4 = @titulartotalplan4 + @conyuguetotalplan4 + @dependientestotalplan4 + @cobrosadicionalestotalplan4
    @tarifatotaltotalplan5 = @titulartotalplan5 + @conyuguetotalplan5 + @dependientestotalplan5 + @cobrosadicionalestotalplan5

    @primanetatotalplan1 = @tarifatotaltotalplan1 + @rpf + @derechopoliza
    @primanetatotalplan2 = @tarifatotaltotalplan2 + @rpf + @derechopoliza
    @primanetatotalplan3 = @tarifatotaltotalplan3 + @rpf + @derechopoliza
    @primanetatotalplan4 = @tarifatotaltotalplan4 + @rpf + @derechopoliza
    @primanetatotalplan5 = @tarifatotaltotalplan5 + @rpf + @derechopoliza

    @ivatotalplan1 = @primanetatotalplan1 * @iva
    @ivatotalplan2 = @primanetatotalplan2 * @iva
    @ivatotalplan3 = @primanetatotalplan3 * @iva
    @ivatotalplan4 = @primanetatotalplan4 * @iva
    @ivatotalplan5 = @primanetatotalplan5 * @iva

    @primaanualtotalplan1 = @primanetatotalplan1 + @ivatotalplan1
    @primaanualtotalplan2 = @primanetatotalplan2 + @ivatotalplan2
    @primaanualtotalplan3 = @primanetatotalplan3 + @ivatotalplan3
    @primaanualtotalplan4 = @primanetatotalplan4 + @ivatotalplan4
    @primaanualtotalplan5 = @primanetatotalplan5 + @ivatotalplan5

    @primerrecibototalplan1 = (@tarifatotaltotalplan1 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerrecibototalplan2 = (@tarifatotaltotalplan2 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerrecibototalplan3 = (@tarifatotaltotalplan3 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerrecibototalplan4 = (@tarifatotaltotalplan4 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)
    @primerrecibototalplan5 = (@tarifatotaltotalplan5 / (@formapago)*(1+@rpf)+@derechopoliza)*(1+@iva)

    if @formapago != 1
      @recibossubsequentestotalplan1 = (@tarifatotaltotalplan1 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentestotalplan2 = (@tarifatotaltotalplan2 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentestotalplan3 = (@tarifatotaltotalplan3 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentestotalplan4 = (@tarifatotaltotalplan4 / (@formapago)*(1+@rpf))*(1+@iva)
      @recibossubsequentestotalplan5 = (@tarifatotaltotalplan5 / (@formapago)*(1+@rpf))*(1+@iva)
    else
      @recibossubsequentestotalplan1 = 0
      @recibossubsequentestotalplan2 = 0
      @recibossubsequentestotalplan3 = 0
      @recibossubsequentestotalplan4 = 0
      @recibossubsequentestotalplan5 = 0
    end
    

  end


  def gastosmedicos
    @layout = Layout.new
  end

  def dbtest
  
    fm = Rfm::Server.new(:host => "portal.veseguro.com",
                        :account_name => "form",
                        :password => "form",
                        :database => "Astro V1009",
                        :ssl => false)
    
    # if we were passed an :id param, it is the sort field, otherwise we find with no sort
    # because we store the results (an Rfm::RecordSet object) in an instance variable
    # (@records) they will be available on the web page.
   
      @records = fm["Astro V1009"]["prospectos_gmm"]
     
   
  end

end