class AglayoutsController < ApplicationController
  # GET /aglayouts
  # GET /aglayouts.json
  
  def create
    @aglayout = Aglayout.new(params[:aglayout])

    respond_to do |format|
      if @aglayout.save
        format.html { redirect_to gracias_path, notice: 'Aglayout was successfully created.' }
        format.json { render json: @aglayout, status: :created, location: @aglayout }
      else
        format.html { render action: "new" }
        format.json { render json: @aglayout.errors, status: :unprocessable_entity }
      end
    end
  end

 
end


