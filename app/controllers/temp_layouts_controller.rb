
class TempLayoutsController < ApplicationController
	def create
    @layout = TempLayout.new(params[:layout])

    respond_to do |format|
      if @layout.save
        format.html { redirect_to gracias_path }
        format.json { render json: @layout, status: :created, location: @layout }
      else
        format.html { render action: "new" }
        format.json { render json: @layout.errors, status: :unprocessable_entity }
      end
    end
  end

end