class LookupTablePreciosController < ApplicationController
  # GET /lookup_table_precios
  # GET /lookup_table_precios.json
  def index
    @lookup_table_precios = LookupTablePrecio.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @lookup_table_precios }
    end
  end

  # GET /lookup_table_precios/1
  # GET /lookup_table_precios/1.json
  def show
    @lookup_table_precio = LookupTablePrecio.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @lookup_table_precio }
    end
  end

  # GET /lookup_table_precios/new
  # GET /lookup_table_precios/new.json
  def new
    @lookup_table_precio = LookupTablePrecio.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @lookup_table_precio }
    end
  end

  # GET /lookup_table_precios/1/edit
  def edit
    @lookup_table_precio = LookupTablePrecio.find(params[:id])
  end

  # POST /lookup_table_precios
  # POST /lookup_table_precios.json
  def create
    @lookup_table_precio = LookupTablePrecio.new(params[:lookup_table_precio])

    respond_to do |format|
      if @lookup_table_precio.save
        format.html { redirect_to @lookup_table_precio, notice: 'Lookup table precio was successfully created.' }
        format.json { render json: @lookup_table_precio, status: :created, location: @lookup_table_precio }
      else
        format.html { render action: "new" }
        format.json { render json: @lookup_table_precio.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /lookup_table_precios/1
  # PUT /lookup_table_precios/1.json
  def update
    @lookup_table_precio = LookupTablePrecio.find(params[:id])

    respond_to do |format|
      if @lookup_table_precio.update_attributes(params[:lookup_table_precio])
        format.html { redirect_to @lookup_table_precio, notice: 'Lookup table precio was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @lookup_table_precio.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lookup_table_precios/1
  # DELETE /lookup_table_precios/1.json
  def destroy
    @lookup_table_precio = LookupTablePrecio.find(params[:id])
    @lookup_table_precio.destroy

    respond_to do |format|
      format.html { redirect_to lookup_table_precios_url }
      format.json { head :no_content }
    end
  end
end
