require 'rfm'
class LayoutsController < ApplicationController
	def create
    @layout = Layout.new(params[:layout])

    respond_to do |format|
      if @layout.save
	    fm = Rfm::Server.new(:host => "portal.veseguro.com",
	                        :account_name => "form",
	                        :password => "form",
	                        :database => "Astro V1009",
	                        :ssl => false)
	  	@db = fm["Astro V1009"]["Prospectos_GMM"] 
	  	@rec = @db.create({:nombre => @layout.nombre,
	  						:email => @layout.email,
	  						:celular => @layout.celular,
	  						:estadoderesidencia => @layout.estadoderesidencia ,
							:edadetitular => @layout.edadetitular ,
							:edaddeconyugue => @layout.edaddeconyugue ,
							:numdehijosconedadde24ymenores => @layout.numdehijosconedadde24ymenores ,
							:formadepago => @layout.formadepago, 
							:edaddehijo1mayora24 => @layout.edaddehijo1mayora24 , 
							:edaddehijo2mayora24 => @layout.edaddehijo2mayora24 ,
							:edaddehijo3mayora24 => @layout.edaddehijo3mayora24 ,
							:edaddehijo4mayora24 => @layout.edaddehijo4mayora24 , 
							:edaddelpadredeltitular => @layout.edaddelpadredeltitular , 
							:edaddelamadredeltitular => @layout.edaddelamadredeltitular , 
							:edaddelasuegradeltitular => @layout.edaddelasuegradeltitular ,
							:edaddelasuegrodeltitular =>@layout.edaddelasuegrodeltitular ,
							:viajes => @layout.viajes,
							:cashemergencia => @layout.cashemergencia
})
	  	 

	  	
        format.html { redirect_to cotizacionpalig_path }
        format.json { render json: @layout, status: :created, location: @layout }
      else
        format.html { render action: "new" }
        format.json { render json: @layout.errors, status: :unprocessable_entity }
      end
    end
  end




end