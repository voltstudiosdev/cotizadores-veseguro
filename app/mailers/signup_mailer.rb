class SignupMailer < ActionMailer::Base
  default from: "noreply@veseguro.com"

  def signup_email(layout)
    @layout = layout
    
    mail(to: 'direccion@veseguro.com', subject: 'Un nuevo prospecto de gastos médicos')
  end
end
