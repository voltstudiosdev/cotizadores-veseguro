require 'test_helper'

class PlansControllerTest < ActionController::TestCase
  setup do
    @plan = plans(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:plans)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create plan" do
    assert_difference('Plan.count') do
      post :create, plan: { acompanante_noche: @plan.acompanante_noche, ambulancia: @plan.ambulancia, area_cobertura: @plan.area_cobertura, cobertura_maxima_anual_asegurado: @plan.cobertura_maxima_anual_asegurado, complicaciones_embarazo: @plan.complicaciones_embarazo, deducible: @plan.deducible, elim_deducible: @plan.elim_deducible, estudios_laboratorio: @plan.estudios_laboratorio, examenes_sin_patologia: @plan.examenes_sin_patologia, extension_coberturas: @plan.extension_coberturas, honorarios_medicos: @plan.honorarios_medicos, hospitalizacion: @plan.hospitalizacion, maternidad: @plan.maternidad, medicamentos: @plan.medicamentos, podologia: @plan.podologia, reduc_deducible: @plan.reduc_deducible, terapias: @plan.terapias, trasplante_organos: @plan.trasplante_organos }
    end

    assert_redirected_to plan_path(assigns(:plan))
  end

  test "should show plan" do
    get :show, id: @plan
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @plan
    assert_response :success
  end

  test "should update plan" do
    put :update, id: @plan, plan: { acompanante_noche: @plan.acompanante_noche, ambulancia: @plan.ambulancia, area_cobertura: @plan.area_cobertura, cobertura_maxima_anual_asegurado: @plan.cobertura_maxima_anual_asegurado, complicaciones_embarazo: @plan.complicaciones_embarazo, deducible: @plan.deducible, elim_deducible: @plan.elim_deducible, estudios_laboratorio: @plan.estudios_laboratorio, examenes_sin_patologia: @plan.examenes_sin_patologia, extension_coberturas: @plan.extension_coberturas, honorarios_medicos: @plan.honorarios_medicos, hospitalizacion: @plan.hospitalizacion, maternidad: @plan.maternidad, medicamentos: @plan.medicamentos, podologia: @plan.podologia, reduc_deducible: @plan.reduc_deducible, terapias: @plan.terapias, trasplante_organos: @plan.trasplante_organos }
    assert_redirected_to plan_path(assigns(:plan))
  end

  test "should destroy plan" do
    assert_difference('Plan.count', -1) do
      delete :destroy, id: @plan
    end

    assert_redirected_to plans_path
  end
end
