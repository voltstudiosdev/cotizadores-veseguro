require 'test_helper'

class LookupTablePreciosControllerTest < ActionController::TestCase
  setup do
    @lookup_table_precio = lookup_table_precios(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lookup_table_precios)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lookup_table_precio" do
    assert_difference('LookupTablePrecio.count') do
      post :create, lookup_table_precio: { edad: @lookup_table_precio.edad, llave: @lookup_table_precio.llave, pan-mx-i: @lookup_table_precio.pan-mx-i, pan-mx-ii: @lookup_table_precio.pan-mx-ii, pan-mx-iii: @lookup_table_precio.pan-mx-iii, pan-mx-iv: @lookup_table_precio.pan-mx-iv, pan-mx-v: @lookup_table_precio.pan-mx-v, producto: @lookup_table_precio.producto, zona: @lookup_table_precio.zona }
    end

    assert_redirected_to lookup_table_precio_path(assigns(:lookup_table_precio))
  end

  test "should show lookup_table_precio" do
    get :show, id: @lookup_table_precio
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lookup_table_precio
    assert_response :success
  end

  test "should update lookup_table_precio" do
    put :update, id: @lookup_table_precio, lookup_table_precio: { edad: @lookup_table_precio.edad, llave: @lookup_table_precio.llave, pan-mx-i: @lookup_table_precio.pan-mx-i, pan-mx-ii: @lookup_table_precio.pan-mx-ii, pan-mx-iii: @lookup_table_precio.pan-mx-iii, pan-mx-iv: @lookup_table_precio.pan-mx-iv, pan-mx-v: @lookup_table_precio.pan-mx-v, producto: @lookup_table_precio.producto, zona: @lookup_table_precio.zona }
    assert_redirected_to lookup_table_precio_path(assigns(:lookup_table_precio))
  end

  test "should destroy lookup_table_precio" do
    assert_difference('LookupTablePrecio.count', -1) do
      delete :destroy, id: @lookup_table_precio
    end

    assert_redirected_to lookup_table_precios_path
  end
end
