require 'test_helper'

class AglayoutsControllerTest < ActionController::TestCase
  setup do
    @aglayout = aglayouts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:aglayouts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create aglayout" do
    assert_difference('Aglayout.count') do
      post :create, aglayout: {  }
    end

    assert_redirected_to aglayout_path(assigns(:aglayout))
  end

  test "should show aglayout" do
    get :show, id: @aglayout
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @aglayout
    assert_response :success
  end

  test "should update aglayout" do
    put :update, id: @aglayout, aglayout: {  }
    assert_redirected_to aglayout_path(assigns(:aglayout))
  end

  test "should destroy aglayout" do
    assert_difference('Aglayout.count', -1) do
      delete :destroy, id: @aglayout
    end

    assert_redirected_to aglayouts_path
  end
end
