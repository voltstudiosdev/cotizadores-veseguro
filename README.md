# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This application is for an insurance firm. The agents of the insurance firm are able to input simple user parameters and immediately see an estimated quote for several options of insurance plans/policies. 

This application pulls from a back-end database and utilizes algorithms that compare policies against several criteria to create the final estimation of cost and coverage. 

The algorithms are developed in-house, so you will not find them in any other location or application.   


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact