# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'csv' 
preferred_access = Plan.create(nombre: 'Preferred Access',
								cobertura_maxima_anual_asegurado: 'USD $2,000,000',
								area_cobertura: 'Libre elección en México y Resto del Mundo, Estados Unidos de América en Red de Proveedores Preferidos', 
								deducible: 'Un Deducible por persona al año, máximo dos por Núcleo Familiar.', 
								reduc_deducible:'Planes I, II y III',
								elim_deducible:' ',
								hospitalizacion:'100%', 
								honorarios_medicos:'100%', 
								estudios_laboratorio:'100%',
								acompanante_noche:'USD $100',
								maternidad:'USD $4,000', 
								complicaciones_embarazo:'USD $500,000', 
								trasplante_organos:'USD $450,000', 
								medicamentos:'USD $50,000',
								ambulancia:'USD $50,000', 
								examenes_sin_patologia:'No Aplica', 
								terapias:'No Aplica',
								podologia:'No Aplica',
								extension_coberturas:'Un (1) año de cobertura continua para los dependientes' )



world_access = Plan.create(nombre: 'World Access',
				cobertura_maxima_anual_asegurado: 'USD $5,000,000',
				area_cobertura: 'Libre elección en México y el Resto del Mundo', 
				deducible: 'Un Deducible por persona al año, máximo dos por Núcleo Familiar.', 
				reduc_deducible:'Planes I, II y III',
				elim_deducible:' ',
				hospitalizacion:'100%', 
				honorarios_medicos:'100%', 
				estudios_laboratorio:'100%',
				acompanante_noche:'USD $300',
				maternidad:'USD $7,500', 
				complicaciones_embarazo:'USD $1,000,000 ', 
				trasplante_organos:'USD $1,000,000 ', 
				medicamentos:'100%',
				ambulancia:'USD $150,000', 
				examenes_sin_patologia:'USD $300', 
				terapias:'100%',
				podologia:'100%',
				extension_coberturas:'Dos (2) años de cobertura continua para los dependientes')


file = Rails.root + "app/assets/datos.csv"
CSV.foreach(file) do |row|
	producto= row[0]
	zona = row[1].to_i
	edad = row[2].to_i
	llave = row[3]
	pan_mx_i = row[4].to_f
	pan_mx_ii = row[5].to_f
	pan_mx_iii = row[6].to_f
	pan_mx_iv = row[7].to_f
	pan_mx_v = row[8].to_f
	
	LookupTablePrecio.create(producto: producto, zona: zona, edad: edad, llave: llave, pan_mx_i: pan_mx_i, pan_mx_ii: pan_mx_ii, pan_mx_iii: pan_mx_iii, pan_mx_iv: pan_mx_iv, pan_mx_v: pan_mx_v)
	
end


file2 = Rails.root + "app/assets/datos-bupa.csv"
CSV.foreach(file2) do |row|
	edad= row[0]
	plan1 = row[1].gsub(/,/, '').to_f
	plan2 = row[2].gsub(/,/, '').to_f
	plan3 = row[3].gsub(/,/, '').to_f
	plan4 = row[4].gsub(/,/, '').to_f
	plan5 = row[5].gsub(/,/, '').to_f
	zona = row[6].to_i
	producto = row[7]
	
	BupaLookupPrecio.create(producto: producto, zona: zona, edad: edad, plan1: plan1, plan2: plan2, plan3: plan3, plan4: plan4, plan5: plan5)
	
end
