class AddViajesToLayouts < ActiveRecord::Migration
  def change
  	add_column :layouts, :viajes, :string
  	add_column :layouts, :cashemergencia, :string
  end
end
