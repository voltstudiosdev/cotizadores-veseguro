class CreateAglayouts < ActiveRecord::Migration
  def change
    create_table :aglayouts do |t|
    	t.string :nombre
	      t.string :celular
	      t.string :email
	    	t.string :estadoderesidencia
	      t.string :edadetitular
	      t.string :edaddeconyugue
	      t.string :numdehijosconedadde24ymenores
	      t.string :formadepago
	      t.string :edaddehijo1mayora24
	      t.string :edaddehijo2mayora24
	      t.string :edaddehijo3mayora24
	      t.string :edaddehijo4mayora24
	      t.string :edaddelpadredeltitular
	      t.string :edaddelamadredeltitular
	      t.string :edaddelasuegradeltitular
	      t.string :edaddelasuegrodeltitular
	      t.string :plan
	      t.string :viajes
	      t.string :cashemergencia

      t.timestamps
    end
  end
end
