class CreateBupaLookupPrecios < ActiveRecord::Migration
  def change
    create_table :bupa_lookup_precios do |t|
      t.string :edad
      t.float :plan1
      t.float :plan2
      t.float :plan3
      t.float :plan4
      t.float :plan5
      t.integer :zona
      t.string :producto

      t.timestamps
    end
  end
end
