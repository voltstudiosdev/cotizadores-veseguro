class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.string :nombre 
      t.string :cobertura_maxima_anual_asegurado
      t.string :area_cobertura
      t.string :deducible
      t.string :reduc_deducible
      t.string :elim_deducible
      t.string :hospitalizacion
      t.string :honorarios_medicos
      t.string :estudios_laboratorio
      t.string :acompanante_noche
      t.string :maternidad
      t.string :complicaciones_embarazo
      t.string :trasplante_organos
      t.string :medicamentos
      t.string :ambulancia
      t.string :examenes_sin_patologia
      t.string :terapias
      t.string :podologia
      t.string :extension_coberturas

      t.timestamps
    end
  end
end
