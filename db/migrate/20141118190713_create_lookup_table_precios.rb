class CreateLookupTablePrecios < ActiveRecord::Migration
  def change
    create_table :lookup_table_precios do |t|
      t.string :producto
      t.integer :zona
      t.integer :edad
      t.string :llave
      t.float :pan_mx_i
      t.float :pan_mx_ii
      t.float :pan_mx_iii
      t.float :pan_mx_iv
      t.float :pan_mx_v

      t.timestamps
    end
  end
end
