# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20141215194258) do

  create_table "aglayouts", :force => true do |t|
    t.string   "nombre"
    t.string   "celular"
    t.string   "email"
    t.string   "estadoderesidencia"
    t.string   "edadetitular"
    t.string   "edaddeconyugue"
    t.string   "numdehijosconedadde24ymenores"
    t.string   "formadepago"
    t.string   "edaddehijo1mayora24"
    t.string   "edaddehijo2mayora24"
    t.string   "edaddehijo3mayora24"
    t.string   "edaddehijo4mayora24"
    t.string   "edaddelpadredeltitular"
    t.string   "edaddelamadredeltitular"
    t.string   "edaddelasuegradeltitular"
    t.string   "edaddelasuegrodeltitular"
    t.string   "plan"
    t.string   "viajes"
    t.string   "cashemergencia"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "bupa_lookup_precios", :force => true do |t|
    t.string   "edad"
    t.float    "plan1"
    t.float    "plan2"
    t.float    "plan3"
    t.float    "plan4"
    t.float    "plan5"
    t.integer  "zona"
    t.string   "producto"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "layouts", :force => true do |t|
    t.string   "nombre"
    t.string   "celular"
    t.string   "email"
    t.string   "estadoderesidencia"
    t.string   "edadetitular"
    t.string   "edaddeconyugue"
    t.string   "numdehijosconedadde24ymenores"
    t.string   "formadepago"
    t.string   "edaddehijo1mayora24"
    t.string   "edaddehijo2mayora24"
    t.string   "edaddehijo3mayora24"
    t.string   "edaddehijo4mayora24"
    t.string   "edaddelpadredeltitular"
    t.string   "edaddelamadredeltitular"
    t.string   "edaddelasuegradeltitular"
    t.string   "edaddelasuegrodeltitular"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.string   "plan"
    t.string   "viajes"
    t.string   "cashemergencia"
  end

  create_table "lookup_table_precios", :force => true do |t|
    t.string   "producto"
    t.integer  "zona"
    t.integer  "edad"
    t.string   "llave"
    t.float    "pan_mx_i"
    t.float    "pan_mx_ii"
    t.float    "pan_mx_iii"
    t.float    "pan_mx_iv"
    t.float    "pan_mx_v"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "plans", :force => true do |t|
    t.string   "nombre"
    t.string   "cobertura_maxima_anual_asegurado"
    t.string   "area_cobertura"
    t.string   "deducible"
    t.string   "reduc_deducible"
    t.string   "elim_deducible"
    t.string   "hospitalizacion"
    t.string   "honorarios_medicos"
    t.string   "estudios_laboratorio"
    t.string   "acompanante_noche"
    t.string   "maternidad"
    t.string   "complicaciones_embarazo"
    t.string   "trasplante_organos"
    t.string   "medicamentos"
    t.string   "ambulancia"
    t.string   "examenes_sin_patologia"
    t.string   "terapias"
    t.string   "podologia"
    t.string   "extension_coberturas"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
  end

  create_table "temp_layouts", :force => true do |t|
    t.string   "nombre"
    t.string   "celular"
    t.string   "email"
    t.string   "estadoderesidencia"
    t.string   "edadetitular"
    t.string   "edaddeconyugue"
    t.string   "numdehijosconedadde24ymenores"
    t.string   "formadepago"
    t.string   "edaddehijo1mayora24"
    t.string   "edaddehijo2mayora24"
    t.string   "edaddehijo3mayora24"
    t.string   "edaddehijo4mayora24"
    t.string   "edaddelpadredeltitular"
    t.string   "edaddelamadredeltitular"
    t.string   "edaddelasuegradeltitular"
    t.string   "edaddelasuegrodeltitular"
    t.string   "plan"
    t.string   "viajes"
    t.string   "cashemergencia"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

end
